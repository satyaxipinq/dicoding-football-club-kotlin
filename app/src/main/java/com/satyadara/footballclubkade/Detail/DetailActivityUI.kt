package com.satyadara.footballclubkade.Detail

import android.support.constraint.ConstraintSet.PARENT_ID
import android.view.View
import com.satyadara.footballclubkade.R
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class DetailActivityUI : AnkoComponent<DetailActivity> {
    companion object {
        val logoDipSize = 250
        val ivLogo = 808080
        val tvDescription = 909090
    }

    override fun createView(ui: AnkoContext<DetailActivity>): View = with(ui) {
        constraintLayout {
            padding = dip(8)

            imageView {
                id = ivLogo
                setImageResource(R.drawable.img_barca)
            }.lparams(logoDipSize, logoDipSize) {
                topToTop = PARENT_ID
                leftToLeft = PARENT_ID
                rightToRight = PARENT_ID
            }

            textView {
                id = tvDescription
                text = "Description"
                textSize = sp(20).toFloat()
            }.lparams(matchParent, wrapContent) {
                topToBottom = ivLogo
                leftToLeft = PARENT_ID
                rightToRight = PARENT_ID
            }
        }
    }
}